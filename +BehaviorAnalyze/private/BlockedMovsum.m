function Data = BlockedMovsum(Data,BlockSize)
Data(end-mod(numel(Data),BlockSize)+1:end)=[];
Data=sum(reshape(Data,BlockSize,[]),1);