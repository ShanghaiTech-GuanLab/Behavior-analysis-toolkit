行为分析工具箱，依赖[数据格式规范](https://gitee.com/ShanghaiTech-GuanLab/Data-format-specification)和[埃博拉酱的图像处理工具箱](https://ww2.mathworks.cn/matlabcentral/fileexchange/99359-video-pixel-measure-by-roi)

所有函数均在BehaviorAnalyze包下，使用前需导入：
```MATLAB
import BehaviorAnalyze.*
```
- [BehF10_BehM10](#BehF10_BehM10) 将BehF1.0表读入为BehM1.0内存格式
- [BlinkM10_HitRates](#BlinkM10_HitRates) 在BlinkM1.0表的Calendar子表中添加一列，记录本次实验的最终命中率
- [BlinkM11_MeanSemLine](#BlinkM11_MeanSemLine) 从BlinkM1.1表生成平均值/标准误折线
- [BlinkM11_RoiExchange](#BlinkM11_RoiExchange) 将BlinkM1.1表的Calendar.Measurements的每个矩阵的两行交换
- [BlinkPlot](#BlinkPlot) 将光闭眼视频的测量值作图，展示光、喷气、闭眼的时相关系
- [FitLognormalCurve](#FitLognormalCurve) 根据样本的均值和方差，生成一条对数正态分布曲线
- [HeterogenousBlockedMeanSem](#HeterogenousBlockedMeanSem) 在尺寸不同的数组之间求分块平均值和总体标准误
- [TimeTable_HMVector](#TimeTable_HMVector) 统计时间表中命中和错失的数目
- [VideoBatchMeasure](#VideoBatchMeasure) 批量测量光闭眼实验视频
# BehF10_BehM10
将BehF1.0表读入为BehM1.0内存格式

输入参数：BehF10Paths(:,1)string，[BehF1.0文件](https://gitee.com/ShanghaiTech-GuanLab/Data-format-specification/blob/master/+FormatSpecification/Specifications/BehF1.0.json)路径，默认打开文件选择对话框要求用户手动选择

返回值：BehM10，[BehM1.0表](https://gitee.com/ShanghaiTech-GuanLab/Data-format-specification/blob/master/+FormatSpecification/Specifications/BehM1.0.json)
# BlinkM10_HitRates
在BlinkM1.0表的Calendar子表中添加一列，记录本次实验的最终命中率

观察蓝光熄灭后、喷气之前的这段时间，最大的闭眼像素值；然后把蓝光亮和亮之前同样时间的像素值作为基线；如果响应窗像素值的最大值大于基线最大值，就认为是一次对蓝光的响应。这样将每个回合二元化，可以求得一个命中率。

## 可选位置参数
BlinkM10(:,3)table，[BlinkM1.0表](https://gitee.com/ShanghaiTech-GuanLab/Data-format-specification/blob/v2.4.0/+FormatSpecification/Specifications/BlinkM1.0.json)，默认打开文件选择对话框要求用户手动选择包含此表的.mat文件
## 名称值参数
FrameRate(1,1)single=30，视频帧率

LightDuration(1,1)single=50，闪光的毫秒数

AirDelay(1,1)single=300，从闪光开始到喷气开始经过的毫秒数

ROI1Light(1,1)logical=true，指示是否Measurement矩阵第1行是Light而不是Eye曲线
## 返回值
BlinkM10(:,3)table，其Calendar子表添加了一列BlinkRates
# BlinkPlot
将光闭眼视频的测量值作图，展示光、喷气、闭眼的时相关系

## 示例
```MATLAB
%选择BlinkM11数据表文件载入
BehaviorAnalyze.BlinkPlot(MATLAB.General.Load().SessionTable.Calendar{1}.Measurements{1});
%显示图窗，观察灯亮、闭眼、喷气三者的时相是否正确。
```
## 位置参数
Measurements(2,:)，第1行是眼睛测量值，第2行是灯光测量值
## 名称值参数
AirDelay(1,1)，从光亮到喷气的延迟

FrameRate(1,1)，视频帧率
# BlinkM11_MeanSemLine
从BlinkM1.1表生成平均值/标准误折线
## 语法
```MATLAB
[Mean,Sem]=BehaviorAnalyze.BlinkM11_MeanSemLine(BlinkM11);
```
## 输入参数
BlinkM11(:,3)table，符合[BlinkM1.1规范](https://gitee.com/ShanghaiTech-GuanLab/Data-format-specification/blob/master/+FormatSpecification/Specifications/BlinkM1.1.json)的数据表，必须具有Calendar.BlinkRates可选列
## 返回值
Mean(:,1)double，平均值折线
Sem(:,1)double，标准误折线
# BlinkM11_RoiExchange
将BlinkM1.1表的Calendar.Measurements的每个矩阵的两行交换

[BlinkM1.1表](https://gitee.com/ShanghaiTech-GuanLab/Data-format-specification/blob/master/+FormatSpecification/Specifications/BlinkM1.1.json)的Calendar.Measurements的每个矩阵的两行具有固定的意义，第1行是眼睛，第2行是灯。如果数据不慎搞反了，可以用本函数换回来。

输入参数：BlinkM11(:,3)table，待交换的BlinkM1.1表

返回值：BlinkM11(:,3)table，交换后的BlinkM1.1表
# FitLognormalCurve
根据样本的均值和方差，生成一条对数正态分布曲线
## 输入参数
Samples(:,1)numeric，样本值

Xs(1,:)numeric，分布曲线的横坐标
## 返回值
Curve(1,:)double，分布曲线的纵坐标
# HeterogenousBlockedMeanSem
在尺寸不同的数组之间求分块平均值和总体标准误

尺寸不同的数组之间一般不能直接算平均值和总体标准误。将每个数组在其每个维度的起始端对齐，不足部分补0，求其平均值数组和总体标准误数组。其中N值取在该位置有实际值的数组个数，因此得到数组的不同位置具有不同的N值。对于向量，还支持分块操作，例如每5个Trial作为一个Block算平均和标准误
## 输入参数
HeterogenousData cell，每个元胞里包含一个数组。将在不同元胞的数组之间求统计量，数组内部不作统计。

BlockSize(1,1)uint8=1，分块尺寸，将在块内求平均值和标准误。如果大于1，无论输入数组为何，输出数组都将为行向量。
## 返回值
Mean，平均值数组，各维尺寸为输入数组各维尺寸的最大值保证适配。

Sem，总体标准误数组，尺寸同Mean。

Number，上述两个统计量数组，其各元素的计算实际参与的数组个数。
# TimeTable_HMVector
统计时间表中命中和错失的数目
## 输入参数
TimeTable tabular，必需，可以是普通表或时间表

HitTag(1,1)string="命中"，可选，表示命中的时间表事件标签

MissTag(1,1)string="错失"，可选，表示错失的时间表事件标签
## 返回值
HMVector(:,1)logical，命中向量，列出具体的每个回合是否命中

NoHits(1,1)double，命中的次数

NoMiss(1,1)double，错失的次数
# VideoBatchMeasure
批量测量光闭眼实验视频

本函数的输入参数和ImageProcessing.VideoBatchMeasure完全相同，主要是增加了额外任务特定功能：解析视频文件的规范文件名
```
<鼠名>.<实验日期>.<会话名>.mp4
```
然后和测量值一起组合成BlinkM1.1格式数据表，保存到视频所在目录下，命名为BlinkM11.mat