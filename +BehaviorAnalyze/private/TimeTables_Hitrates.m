function HitRates = TimeTables_Hitrates(TimeTables,BlockSize)
import BehaviorAnalyze.*
[~,~,HMVector]=TimeTable_HMVector(vertcat(TimeTables{:}));
HitRates={HeterogenousBlockedMeanSem({HMVector},BlockSize)};
end