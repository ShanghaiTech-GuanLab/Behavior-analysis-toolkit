function Latency = TimeTable_Latency(TimeTable,StimuliTags,ResponseTags)
Tag=TimeTable.Tag;
Time=TimeTable.Time;
Stimuli=Time(any(Tag==StimuliTags,2));
Response=Time(any(Tag==ResponseTags,2));
if isempty(Stimuli)||isempty(Response)
	Latency=duration.empty(0,1);
else
	Latency=Response-Stimuli(1:numel(Response));
end